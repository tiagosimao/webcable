package main

import (
	"log"
	"time"

	"gitlab.com/tiagosimao/webcable/internal/config"
	"gitlab.com/tiagosimao/webcable/internal/webcable"
)

func main() {
	log.Printf("booting mole\n")

	log.Printf("loading configuration\n")
	config, err := config.LoadConfig()
	if err != nil {
		log.Fatalf("error loading configuration: %s\n", err)
	}
	log.Printf("configuration loaded\n")

	for {
		err = webcable.EnsureConnectable(config)
		if err != nil {
			log.Printf("error running iteration: %s\n", err)
		}
		time.Sleep(config.SleepSeconds)
	}
}
