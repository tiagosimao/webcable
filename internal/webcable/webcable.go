package webcable

import (
	"fmt"

	"codeberg.org/tiagosimao/go-gr241ag/pkg/gr241ag"
	"gitlab.com/tiagosimao/webcable/internal/config"
)

func EnsureConnectable(config config.Config) error {
	virtualServers := []gr241ag.VirtualServer{
		{
			ServerName:        fmt.Sprintf("http://%s", config.PublicHostname),
			Protocol:          "TCP",
			ExternalPortStart: 80,
			ExternalPortEnd:   80,
			InternalPortStart: 80,
			InternalPortEnd:   80,
			ServerIpAddress:   config.LocalIpv4,
			WanInterface:      "veip0.1",
		},
		{
			ServerName:        fmt.Sprintf("https://%s", config.PublicHostname),
			Protocol:          "TCP",
			ExternalPortStart: 443,
			ExternalPortEnd:   443,
			InternalPortStart: 443,
			InternalPortEnd:   443,
			ServerIpAddress:   config.LocalIpv4,
			WanInterface:      "veip0.1",
		},
	}
	routerClient := gr241ag.Client{
		Address:  config.RouterAddress,
		Port:     22,
		Username: config.RouterUser,
		Password: config.RouterPass,
	}
	_, err := routerClient.Nat().VirtualServers().PutAll(virtualServers)
	return err
}
