package config

import (
	"fmt"
	"os"
	"strconv"
	"time"
)

const sleepSecondsEnvName = "WEBCABLE_SLEEP_SECONDS"
const sleepSecondsDefault = 60
const sleepSecondsMin = 1
const sleepSecondsMax = 86400

const publicHostnameEnvName = "WEBCABLE_PUBLIC_HOSTNAME"

const localIpv4EnvName = "WEBCABLE_LOCAL_IPV4"

const routerAddressEnvName = "WEBCABLE_ROUTER_ADDRESS"
const routerUserEnvName = "WEBCABLE_ROUTER_USER"
const routerPassEnvName = "WEBCABLE_ROUTER_PASS"

type Config struct {
	SleepSeconds   time.Duration
	PublicHostname string
	LocalIpv4      string
	RouterAddress  string
	RouterUser     string
	RouterPass     string
}

func LoadConfig() (Config, error) {
	config := Config{}

	sleepSecondsStringFromEnv, isSet := os.LookupEnv(sleepSecondsEnvName)
	if isSet {
		sleepSecondsFromEnv, err := strconv.Atoi(sleepSecondsStringFromEnv)
		if err != nil {
			return config, fmt.Errorf("error parsing %s: %s", sleepSecondsEnvName, err)
		}
		if sleepSecondsFromEnv < sleepSecondsMin {
			return config, fmt.Errorf("configuration error: sleep seconds cannot be lower than %d", sleepSecondsMin)
		} else if sleepSecondsFromEnv > sleepSecondsMax {
			return config, fmt.Errorf("configuration error: sleep seconds cannot be greater than %d", sleepSecondsMax)
		}
		config.SleepSeconds = time.Duration(sleepSecondsFromEnv) * time.Second
	} else {
		fmt.Printf("configuration for sleep seconds not set: using default (%d)\n", sleepSecondsDefault)
		config.SleepSeconds = time.Duration(sleepSecondsDefault) * time.Second
	}

	localIpv4, isSet := os.LookupEnv(localIpv4EnvName)
	if isSet {
		config.LocalIpv4 = localIpv4
	} else {
		return config, fmt.Errorf("configuration for local IP not found, please set environment variable %s=<my local IP>", localIpv4EnvName)
	}

	publicHostname, isSet := os.LookupEnv(publicHostnameEnvName)
	if isSet {
		config.PublicHostname = publicHostname
	} else {
		return config, fmt.Errorf("configuration for public hostname not found, please set environment variable %s=<www.example.com>", publicHostnameEnvName)
	}

	routerIp, isSet := os.LookupEnv(routerAddressEnvName)
	if isSet {
		config.RouterAddress = routerIp
	} else {
		return config, fmt.Errorf("configuration for router address not found, please set environment variable %s=<my router address>", routerAddressEnvName)
	}

	routerUser, isSet := os.LookupEnv(routerUserEnvName)
	if isSet {
		config.RouterUser = routerUser
	} else {
		return config, fmt.Errorf("configuration for router username not found, please set environment variable %s=<router user>", routerUserEnvName)
	}

	routerPass, isSet := os.LookupEnv(routerPassEnvName)
	if isSet {
		config.RouterPass = routerPass
	} else {
		return config, fmt.Errorf("configuration for router password not found, please set environment variable %s=<router pass>", routerPassEnvName)
	}

	return config, nil
}
