include .env
include .config

run:
	go run cmd/$${NAME}/$${NAME}.go

release:
	git update-index --refresh && git diff-index --quiet HEAD --
	docker run -w /app -v `pwd`:/app psanetra/git-semver:1.1.0 next > .version
	docker buildx create --use
	docker buildx build --build-arg NAME=$${NAME} --platform $${PLATFORMS} -t $${GROUP}/$${NAME}:`cat .version` --push .
	git tag `cat .version`
	git push origin `cat .version`
	rm .version
