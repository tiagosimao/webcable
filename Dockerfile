FROM golang:1.21.6-alpine3.19 as build
ARG NAME
ENV CGO_ENABLED=0
WORKDIR /src
COPY . .
RUN GOARCH=${TARGETARCH} go build -ldflags "-w -s" -o app cmd/${NAME}/${NAME}.go

FROM alpine:3.19.0
ARG TARGETARCH
RUN addgroup --system --gid 1337 app && \
    adduser --system --disabled-password --home /home/app --uid 1337 --ingroup app app
USER app
COPY --from=build /src/app /home/app/app
ENTRYPOINT ["/home/app/app"]