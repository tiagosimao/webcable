module gitlab.com/tiagosimao/webcable

go 1.21.5

require codeberg.org/tiagosimao/go-gr241ag v0.0.0-20231230225512-ab4974f4c33b

require (
	golang.org/x/crypto v0.17.0 // indirect
	golang.org/x/sys v0.15.0 // indirect
)
